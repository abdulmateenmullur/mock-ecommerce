import * as http from 'http'
import { AddressInfo } from 'net'
import { createServer } from './config/express'

const host = process.env.HOST || '0.0.0.0'
const port = process.env.PORT || '5000'

async function startServer() {
  const app = createServer()

  const server = http.createServer(app).listen({ host, port }, () => {
    const addressInfo = server.address() as AddressInfo
    console.log(`Server ready at http://${addressInfo.address}:${addressInfo.port}`)
  })

  const signalTraps: NodeJS.Signals[] = ['SIGTERM', 'SIGINT', 'SIGUSR2']
  signalTraps.forEach(type => {
    process.once(type, async () => {
      console.log(`process.once ${type}`)

      server.close(() => {
        console.log('HTTP server closed')
      })
    })
  })
}

startServer()
