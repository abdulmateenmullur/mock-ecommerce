import { randBrand, randNumber, randProduct, randProductCategory, randUuid } from '@ngneat/falso'
import * as express from 'express'
import * as cors from 'cors'

interface SearchBrand {
  ID: string
  name: string
}

interface SearchCategory {
  ID: string
  name: string
  subCategories: SearchSubCategory[]
}

export interface SearchSubCategory {
  ID: string
  name: string
}

interface SearchFilter {
  categories: SearchCategory[]
  brands: SearchBrand[]
}

const createServer = (): express.Application => {
  const app = express()
  app.use(cors())

  app.use(express.urlencoded({ extended: true }))
  app.use(express.json())

  app.disable('x-powered-by')

  app.get('/health', (_req, res) => {
    res.send('OK')
  })

  app.get('/filters', (_req, res) => {
    const brands: SearchBrand[] = randBrand({ length: 10 }).map(brandName => ({ ID: randUuid(), name: brandName }))
    const categories: SearchCategory[] = randProductCategory({ length: randNumber({ min: 2, max: 4 }) }).map(
      category => {
        const subCategories: SearchSubCategory[] = randProductCategory({ length: randNumber({ min: 2, max: 4 }) }).map(
          subCategory => ({ ID: randUuid(), name: subCategory }),
        )

        return {
          ID: randUuid(),
          name: category,
          subCategories,
        }
      },
    )
    const searchFilter: SearchFilter = {
      categories,
      brands,
    }

    res.json({ searchFilter })
  })

  app.get('/products', (_req, res) => {
    const products = randProduct({ length: 10 })
    res.json({ products })
  })

  return app
}

export { createServer }
